import json
import requests
api_token = 'WoTZzDiiSa5SVyN4k5aZyNNRa1MgRygU'
api_url_base = 'https://www.udemy.com/instructor-api/v1/'

headers = {'Content-Type' : 'application/json',
           'Authorization': 'bearer {0}'.format(api_token)}

def generate_database():
    import sqlite3
    import os
    os.remove("udemy.db")
    conn = sqlite3.connect('udemy.db')
    c = conn.cursor()

    #Create Tables
    c.execute('''CREATE TABLE courses
                (id INTEGER PRIMARY KEY, id_course TEXT, title TEXT,
                url TEXT) 
                ''')
    c.execute('''CREATE TABLE reviews
                (id INTEGER PRIMARY KEY, id_review TEXT, content TEXT, rating INT, created TEXT, user_modified TEXT, user_id TEXt, user_title TEXT, user_name TEXT, course TEXT, id_course TEXT)
                ''')
    conn.commit()
    return conn, c

def get_courses(conn, c):
    '''
    Fields inside :
        - _class
        - id
        - title
        - url
        - is_paid
        - published_title
        - visible_instructors
    '''
    api_url = '{0}taught-courses/courses/'.format(api_url_base)
    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        results = json.loads(response.content.decode('utf-8'))["results"]
        for course in results:
            if course["published_title"] == None:
                pass
            else:
                print("Adding course : {0}".format(course["title"]))
                c.execute('''
                INSERT INTO courses (id_course, title, url) VALUES(?,?,?)
                ''', (course['id'], course['title'], course['url']))
                conn.commit()
    else:
        return None

def get_reviews(conn, c):
    '''
    Fields inside g:
      - id
      - content
      - rating
      - user_modified
      - created
      - user
        - id
        - title
        - name
    '''

    c.execute("SELECT * FROM courses")
    for course in c.fetchall():
        id_course = course[1]
        title_course = course[2]

        api_url = '{0}taught-courses/reviews/?course={1}?ordering=-user_modified&page_size=100'.format(api_url_base, id_course)
        response = requests.get(api_url, headers=headers)

        if response.status_code == 200:
            results = json.loads(response.content.decode('utf-8'))["results"]
            print('Retrieving reviews from course {}'.format(title_course))
            for review in results:
                c.execute('''
                INSERT INTO reviews (id_review, content, rating, created, user_modified, user_id, user_title, user_name, course, id_course) \
                VALUES(?,?,?,?,?,?,?,?,?,?)
                ''', (review['id'], review['content'], review['rating'], review['created'], review['user_modified'], review['user']['id'], review['user']['title'], review['user']['name'], title_course, id_course))
                conn.commit()
    else:
        return None

#print(json.dumps(parsed, indent=4, sort_keys=True))
connexion = generate_database()
conn = connexion[0]
c = connexion[1]
courses = get_courses(conn, c)
reviews = get_reviews(conn, c)
#Supprimer les doublons
c.execute('''
DELETE FROM reviews WHERE id NOT IN (SELECT min(id) FROM reviews GROUP BY id_review)
''')
conn.close()
#c.execute("SELECT * FROM reviews WHERE id=1000000")
#list = c.fetchall()
#if not list:
#    print("NOK")
