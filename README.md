# Code Quality

# Check types
When we are talking about Code Quality, we can check for :
- Duplication (Duplication)
- Bug Risk (fixme)
- Style (pep8)
- Complexity (Radon)
- Security (sonar-python)